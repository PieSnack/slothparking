package com.ghostware.slot2park.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ghostware.slot2park.R;
import com.ghostware.slot2park.manager.DataBookingHistory;
import com.ghostware.slot2park.utils.OnItemClickListener;

import java.util.List;

public class RecyclerViewAdapterHistory extends RecyclerView.Adapter<RecyclerViewAdapterHistory.HistoryViewHolder> implements View.OnClickListener {

    //constructor
    private List<DataBookingHistory> listBookingHistory;

    public RecyclerViewAdapterHistory(List<DataBookingHistory> listBookingHistory) {
        this.listBookingHistory = listBookingHistory;
    }

    @Override
    public void onClick(View view) {

    }
    //constructor

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public static class HistoryViewHolder extends RecyclerView.ViewHolder {

        CardView cvHistory;
        TextView tvPlaceName;
        TextView tvPrice;
        TextView tvDateBookingStart;          //datebooking start
        TextView tvHours;
        ImageView imgViewPlaceHistoryList;

        HistoryViewHolder(View itemView) {
            super(itemView);
            imgViewPlaceHistoryList = itemView.findViewById(R.id.imgviewHistoryPlaceImage);
            cvHistory = itemView.findViewById(R.id.cvHistory);
            tvPlaceName = itemView.findViewById(R.id.tvHistoryPlaceName);
            tvPrice = itemView.findViewById(R.id.tvHistoryPrice);
            tvHours = itemView.findViewById(R.id.tvHistoryHours);
            tvDateBookingStart = itemView.findViewById(R.id.tvDatebeginBook);
        }
    }

    //implement


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_history, parent, false);
        v.setOnClickListener(this);
        return new HistoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(final HistoryViewHolder holder, int position) {
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.tvPlaceName.setText(listBookingHistory.get(position).placename);
        if (listBookingHistory.get(position).price.equals("0.0")){
            holder.tvPrice.setText("Free Parking");
        }
        else{
            holder.tvPrice.setText(listBookingHistory.get(position).price);
        }
        Log.d("surfer", "onBindViewHolder: "+listBookingHistory.get(position).hours);
        if (listBookingHistory.get(position).hours/60<2) {
            holder.tvHours.setText(String.valueOf(listBookingHistory.get(position).hours / 60) + " hour");
        }else{holder.tvHours.setText(String.valueOf(listBookingHistory.get(position).hours / 60) + " hours");}
            holder.tvDateBookingStart.setText(listBookingHistory.get(position).datebookingstart);
        Glide.with(holder.imgViewPlaceHistoryList.getContext()).load(listBookingHistory.get(position).strimageplceurl).into(holder.imgViewPlaceHistoryList);
        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(holder.getAdapterPosition());
                    Toast.makeText(view.getContext(),"Got holder"+holder.getAdapterPosition(),Toast.LENGTH_SHORT).show();
                }
            }
        });
        //animate(holder);
    }

    @Override
    public int getItemCount() {
        return
        listBookingHistory.size();
    }
    //implement
}
