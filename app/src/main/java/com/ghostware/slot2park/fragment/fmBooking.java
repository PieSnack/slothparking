package com.ghostware.slot2park.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghostware.slot2park.MainActivity;
import com.ghostware.slot2park.manager.UserData;
import com.ghostware.slot2park.model.ModelPlaceData;
import com.ghostware.slot2park.manager.PlaceData;
import com.ghostware.slot2park.manager.DataSearch;
import com.ghostware.slot2park.model.ModelPromotionData;
import com.ghostware.slot2park.model.ModelUserReceiveData;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.bumptech.glide.Glide;
import com.ghostware.slot2park.adapter.RecyclerViewAdapterBooking;
import com.ghostware.slot2park.utils.OnItemClickListener;
import com.ghostware.slot2park.R;
import com.ghostware.slot2park.TransactionActivity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class fmBooking extends Fragment implements ValueEventListener, View.OnClickListener {
    public RecyclerView bookingRecyclerView;
    private List<PlaceData> listPlaceDataBooking;
    private RecyclerViewAdapterBooking rvAdapter;
    private DatabaseReference mRootRef;
    private DatabaseReference mPlaceToPark;
    private DatabaseReference mRootRefPromotion;
    private StorageReference storageRef, storageImgPlaceRef, storagePromotionRef;
    private RecyclerView.ItemAnimator itemAnimator;
    private Button btnBookThis;
    private String strPriceRate = " THB/H";
    private ImageView imgView;
    private TextView tvPlaceTitle;
    private TextView tvPlacePrice;

    private View rootViewBooking;

    //special
    private DataSearch theDataThatCameFromListDataSearch;
    private ImageView imgViewTextScreen;
    private String strPopUp;
    private List<DataSearch> listDataSearch;
    private DatabaseReference mCurrentUser;
    private String username,userphone,usercarlicenseplate,useremail,useraddress;
    private String strProviderId,strUid,strUserId,strName,strEmail,strPhotoUrl;
    private ValueEventListener mCurrentUserVEListener;
    private Uri photoUrl;
    private String userid;
    private Double usermoney;
    private List<UserData> listShortProvidingData;

    public fmBooking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootViewBooking = inflater.inflate(R.layout.fragment_fm_booking, container, false);
        initInstance(rootViewBooking);
        bookingRecyclerView = rootViewBooking.findViewById(R.id.recyclerViewBooking);

        referCurrentUser();
        searchPlaceControl();


        mRootRef = FirebaseDatabase.getInstance().getReference();
        mRootRefPromotion = mRootRef.child("Promotion");
        listShortProvidingData = new ArrayList<>();
        mRootRefPromotion.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelPromotionData promotionData = dataSnapshot.getValue(ModelPromotionData.class);
                String promotionUrl = promotionData.getPromourl();
                Log.d("checkpromo", promotionUrl);
                Glide.with(rootViewBooking.getContext()).load(promotionUrl).into(imgView);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mPlaceToPark = mRootRef.child("PlaceToPark");
        mPlaceToPark.addValueEventListener(this);

        mCurrentUser = mRootRef.child("UserProfile");
        mCurrentUserVEListener = new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot shining : dataSnapshot.getChildren()) {
                    ModelUserReceiveData model = shining.getValue(ModelUserReceiveData.class);
                    username = model.getUsername();
                    usercarlicenseplate = model.getUsercarlicenseplate();
                    userphone = model.getUserphone();
                    useraddress = model.getUseraddress();
                    useremail = model.getUseremail();
                    userid = model.getUserid();
                    usermoney = model.getUsermoney();
                    String strusermoney = String.valueOf(usermoney);
                    if (strUserId.equals(userid)){
                        listShortProvidingData.add(new
                                UserData(
                                userid,
                                username,
                                useremail,
                                userphone,
                                usercarlicenseplate,
                                useraddress,
                                usermoney));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mCurrentUser.addValueEventListener(mCurrentUserVEListener);

        storageRef = FirebaseStorage.getInstance().getReference();
        storageImgPlaceRef = storageRef.child("place_to_park_photos");
        storagePromotionRef = storageRef.child("promotion");

        Log.d("ResultFinishStorageRef", storageImgPlaceRef.getBucket());
        inRecyclerView();
        imgViewTextScreen.setVisibility(View.INVISIBLE);
        btnBookThis.setVisibility(View.INVISIBLE);


        listPlaceDataBooking = new ArrayList<>();
        listDataSearch = new ArrayList<>();
        rvAdapter = new RecyclerViewAdapterBooking(listDataSearch);
        rvAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                imgViewTextScreen.setVisibility(View.VISIBLE);
                btnBookThis.setVisibility(View.VISIBLE);        //While Test
                theDataThatCameFromListDataSearch = listDataSearch.get(position);

                //setImage here
                Glide.with(rootViewBooking.getContext())
                        .load(theDataThatCameFromListDataSearch.url)
                        .into(imgView);

                //setPlaceName here
                tvPlaceTitle.setText(theDataThatCameFromListDataSearch.title);
                //setPlacePrice here
                if (theDataThatCameFromListDataSearch.description.equals("0")) {
                    tvPlacePrice.setText("Free for parking");
                } else {
                    String priceOutPut = theDataThatCameFromListDataSearch.description + strPriceRate;
                    tvPlacePrice.setText(priceOutPut);
                }
                //anyEffect on each Recycler was clicked
            }
        });
        bookingRecyclerView.setAdapter(rvAdapter);
        bookingRecyclerView.setLayoutManager(new LinearLayoutManager(rootViewBooking.getContext()));

        return rootViewBooking;
    }

    private void referCurrentUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                strProviderId = profile.getProviderId();     //*
                strUid = profile.getUid();
                strName = profile.getDisplayName();
                photoUrl = profile.getPhotoUrl();
                strUserId = user.getUid();               //*
                strEmail = user.getEmail();              //*

                strPhotoUrl = "https://graph.facebook.com/" + strUid;

            }
        }
    }

    private void searchPlaceControl() {
        PlaceAutocompleteFragment placeAutocompleteFragment = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                if (listDataSearch!=null){
                    listDataSearch.clear();
                }
                strPopUp = place.getName().toString();

                if (listPlaceDataBooking != null) {
                    int position;
                    //TODO Fixing address on Firebase to lower case
                    //TODO Fix bugs the images and RecyclerViews not same
                    for (position = 0; position < listPlaceDataBooking.size(); position++) {
                        if (listPlaceDataBooking.get(position).address.contains(strPopUp)) {
                            listDataSearch.add(new DataSearch(
                                    listPlaceDataBooking.get(position).title,
                                    listPlaceDataBooking.get(position).description,
                                    listPlaceDataBooking.get(position).url,
                                    listPlaceDataBooking.get(position).latitude,
                                    listPlaceDataBooking.get(position).longitude,
                                    listPlaceDataBooking.get(position).stackremain,
                                    listPlaceDataBooking.get(position).stackfull,
                                    listPlaceDataBooking.get(position).address,
                                    listPlaceDataBooking.get(position).owner,
                                    listPlaceDataBooking.get(position).ownerphone
                            ));
                        }

                    }

                }else {
                    anyToast("No data received");
                }
                rvAdapter.notifyDataSetChanged();
            }


            @Override
            public void onError(Status status) {
                Log.i("place", "An error occurred: " + status);
            }
        });
    }

    private void anyToast(String strPopUp) {
        Toast.makeText(rootViewBooking.getContext(), strPopUp, Toast.LENGTH_SHORT).show();
    }


    private void initInstance(View v) {
        btnBookThis = v.findViewById(R.id.btnBook);
        btnBookThis.setOnClickListener(this);
        imgView = v.findViewById(R.id.imgPlaceToBook);
        imgViewTextScreen = v.findViewById(R.id.imgViewTextScreen);

        //TODO picture of Promotion using FireBase get and put it here
        //imgView.setImageResource(R.drawable.firstcoverimg);

        tvPlaceTitle = v.findViewById(R.id.tvPlaceName);
        tvPlacePrice = v.findViewById(R.id.tvPrice);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            ModelPlaceData modelData = child.getValue(ModelPlaceData.class);

            /*This will receive whole data from ModelData.class and instead value themselves, then we throw them in a list
            After that  we throw a list in Adapter name's rvAdapter*/
            //TODO query data here
            String parkname = modelData.getParkname();
            double latitude = modelData.getDouLatitude();
            double longitude = modelData.getDouLongtitude();
            long price = modelData.getPrice();
            String photourl = modelData.getPhotourl();
            Integer stackfull = modelData.getStackfull();
            Integer stackremain = modelData.getStackremain();
            String address = modelData.getAddress();
            String owner = modelData.getOwner();
            String ownerphone = modelData.getOwnerphone();

            listPlaceDataBooking.add(new PlaceData(parkname, String.valueOf(price), photourl, latitude, longitude, stackfull, stackremain, address,owner,ownerphone));
            rvAdapter.notifyDataSetChanged();

            ((MainActivity) getActivity()).CallFromFragment(listPlaceDataBooking);
        }
    }


    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    private void inRecyclerView() {
        itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        bookingRecyclerView.setItemAnimator(itemAnimator);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (rvAdapter != null) {
            //ClearData
            mPlaceToPark.removeEventListener(this);


        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        //recyclerView onclick
        if (view == btnBookThis) {
            //Toast.makeText(getActivity(), "you click on book", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getActivity(), TransactionActivity.class);
            dataToIntent(intent);
            startActivity(intent);
        }

    }

    private void dataToIntent(Intent intent) {
        //5 Power Ranger
        intent.putExtra("latitude", String.valueOf(theDataThatCameFromListDataSearch.latitude));
        intent.putExtra("longitude", String.valueOf(theDataThatCameFromListDataSearch.longitude));
        intent.putExtra("placename", theDataThatCameFromListDataSearch.title);
        intent.putExtra("price", theDataThatCameFromListDataSearch.description);
        intent.putExtra("imageUrl", theDataThatCameFromListDataSearch.url);
        intent.putExtra("owner",theDataThatCameFromListDataSearch.owner);
        intent.putExtra("ownerphone",theDataThatCameFromListDataSearch.ownerphone);

        intent.putExtra("userid",strUserId);
        intent.putExtra("userphone",userphone);
        intent.putExtra("username",username);
        intent.putExtra("usercarlicenseplate",usercarlicenseplate);
        intent.putExtra("useremail",strEmail);
        intent.putExtra("useraddress",useraddress);
    }

}

