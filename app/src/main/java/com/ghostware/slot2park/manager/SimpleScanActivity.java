package com.ghostware.slot2park.manager;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ghostware.slot2park.R;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class SimpleScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private View mViewDia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_scan);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view

    }


    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("result", rawResult.getText()); // Prints scan results
        Log.v("result", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        final AlertDialog dia = new AlertDialog.Builder(this).create();
        mViewDia = getLayoutInflater().inflate(R.layout.dialog_payhere,null);
        dia.setView(mViewDia);
        Button btnPayParkHere = mViewDia.findViewById(R.id.btnPayParkHere);
        btnPayParkHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sent book to parkhere to firebase
                dia.dismiss();
            }
        });
        Button btnCancelParkhere = mViewDia.findViewById(R.id.btnCancelParkHere);
        btnCancelParkhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dia.dismiss();
            }
        });
        //refer data here

        anyToast(String.valueOf(rawResult));

        dia.show();
        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    private void anyToast(String s) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }
}
