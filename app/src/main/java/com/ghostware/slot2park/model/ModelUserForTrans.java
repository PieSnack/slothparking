package com.ghostware.slot2park.model;

public class ModelUserForTrans {
       public String userid;
       public String username;
       public String useremail;
       public String usercarlicenseplate;
       public String userphone;
       public String useraddress;
       public Double usermoney;


    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public void setUsercarlicenseplate(String usercarlicenseplate) {
        this.usercarlicenseplate = usercarlicenseplate;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public void setUsermoney(Double usermoney) {
        this.usermoney = usermoney;
    }

    public String getUserid() {
        return userid;
    }

    public String getUsername() {
        return username;
    }

    public String getUseremail() {
        return useremail;
    }

    public String getUsercarlicenseplate() {
        return usercarlicenseplate;
    }

    public String getUserphone() {
        return userphone;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public Double getUsermoney() {
        return usermoney;
    }
}
