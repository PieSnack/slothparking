package com.ghostware.slot2park.manager;

import com.ghostware.slot2park.model.ModelUserReceiveData;

public class UserDataReceive extends ModelUserReceiveData {
    public String userid,username,useremail,userphone,usercarlicenseplate,useraddress;
    public Double usermoney;


    public UserDataReceive(String userid, String username, String useremail, String userphone, String usercarlicenseplate, String useraddress, Double usermoney) {
        this.userid = userid;
        this.username = username;
        this.userphone = userphone;
        this.useremail = useremail;
        this.usercarlicenseplate = usercarlicenseplate;
        this.useraddress = useraddress;
        this.usermoney = usermoney;
    }
}
