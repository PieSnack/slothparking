package com.ghostware.slot2park;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.ghostware.slot2park.manager.UserData;
import com.ghostware.slot2park.model.ModelUserReceiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends AppCompatActivity implements View.OnClickListener, ValueEventListener {
    private ImageButton imgBtnBackToMain;
    private ImageView imgUser;
    private String userid, username, photourl, useremail, useraddress, usercarlicenseplate, userphone,strusermoney;
    private Double usermoney;
    private TextView tvUserName, tvMoneyDisplay;
    private Button btnEditTopUp, btnLanguage, btnLogOut;

    private TextView tvEmail, tvCarLicensePlate, tvUserPhoneNo, tvAddress;
    private EditText edtAddress, edtCarLicensePlate, edtUserPhone;

    private Button btnEditInfo;
    private Button btnSaveInfo, btnCancelInfo;
    private List<UserData> listUserData = new ArrayList<>();

    private TextView tvDiaName,tvDiaEmail;
    private int position = 0;
    private Button btnProvincePick;
    private String USER_DATA="User_Config";
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mUsersRef = mRootRef.child("UserProfile");

    private Button btnHistory;
    private String strUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        initInstance();


    }

    //region hide soft key
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(final View anyView) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(anyView instanceof EditText)) {
            anyView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    //hideSoftKeyboard(UserActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (anyView instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) anyView).getChildCount(); i++) {
                View innerView = ((ViewGroup) anyView).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
    //endregion hide soft key

    private void initInstance() {

        imgBtnBackToMain = (ImageButton) findViewById(R.id.imgBtnBackToMain);
        imgBtnBackToMain.setOnClickListener(this);
        imgUser = (ImageView) findViewById(R.id.userImg);

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvMoneyDisplay = (TextView) findViewById(R.id.tvMoneyDisplay);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvCarLicensePlate = (TextView) findViewById(R.id.tvCarID);
        tvUserPhoneNo = (TextView) findViewById(R.id.tvUserPhoneNo);
        tvAddress = (TextView) findViewById(R.id.tvUserAddress);

        btnEditInfo = (Button) findViewById(R.id.btnEditInfo);
        btnEditInfo.setOnClickListener(this);

        btnEditTopUp = (Button) findViewById(R.id.btnTopUp);
        btnEditTopUp.setOnClickListener(this);

        btnHistory = (Button)findViewById(R.id.btnHistory);
        btnHistory.setOnClickListener(this);

        btnLanguage = (Button) findViewById(R.id.btnLanguage);
        btnLanguage.setOnClickListener(this);

        btnLogOut = (Button) findViewById(R.id.btnLogOutApp);
        btnLogOut.setOnClickListener(this);
        gotDataFromFirebase();

        gotDataFromIntent();
        //TODO if FireBase not null pull to these text

        setTextUserInfo();


    }

    private void gotDataFromFirebase() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null) {
            strUserId = user.getUid();
        }
        mUsersRef.addValueEventListener(this);
    }

    private void setTextUserInfo() {
        tvUserName.setText(username);
        tvMoneyDisplay.setText(strusermoney);
        tvEmail.setText(useremail);
        tvUserPhoneNo.setText(userphone);
        tvCarLicensePlate.setText(usercarlicenseplate);
        tvAddress.setText(useraddress);
    }

    private void gotDataFromIntent() {
        //TODO intent from main here
        userid = getIntent().getStringExtra("userid");
        username = getIntent().getStringExtra("username");
        useremail = getIntent().getStringExtra("useremail");
        userphone = getIntent().getStringExtra("userphone");
        useraddress = getIntent().getStringExtra("useraddress");
        usercarlicenseplate = getIntent().getStringExtra("usercarlicenseplate");
        strusermoney = getIntent().getStringExtra("usermoney");
        photourl = getIntent().getStringExtra("photourl");

        Picasso.with(this).load(photourl).into(imgUser);

    }

    //region optional
    private void anyToast(String s) {
        Toast.makeText(UserActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view == imgBtnBackToMain) {
            finish();
        }
        if (view == btnEditInfo) {
            //region edit info
            anyToast("Edit");
            final AlertDialog dia = new AlertDialog.Builder(this).create();
            View mViewDiaInfo = getLayoutInflater().inflate(R.layout.dia_user_information, null);
            dia.setView(mViewDiaInfo);
            //TODO set soft keyboard hide when press out of EditText

            tvDiaName = mViewDiaInfo.findViewById(R.id.tvDiaName);
            tvDiaEmail = mViewDiaInfo.findViewById(R.id.tvDiaEmail);
            edtUserPhone = mViewDiaInfo.findViewById(R.id.edtDiaPhone);
            edtCarLicensePlate = mViewDiaInfo.findViewById(R.id.edtCarLicensePlate);
            edtAddress = mViewDiaInfo.findViewById(R.id.edtAddress);

            tvDiaName.setText(username);
            tvDiaEmail.setText(useremail);
            edtUserPhone.setText(userphone);
            edtCarLicensePlate.setText(usercarlicenseplate);
            edtAddress.setText(useraddress);


            //region Province Button
            btnProvincePick = mViewDiaInfo.findViewById(R.id.btnProvincePick);
            btnProvincePick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                anyToast("Province Pick");
                }
            });
            //endregion Province Button

            //region Info button
            btnSaveInfo = mViewDiaInfo.findViewById(R.id.btnSaveInfo);
            btnSaveInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //setText

                    tvUserPhoneNo.setText(edtUserPhone.getText());
                    tvCarLicensePlate.setText(edtCarLicensePlate.getText());
                    tvAddress.setText(edtAddress.getText());

                    userphone = tvUserPhoneNo.getText().toString();
                    usercarlicenseplate = tvCarLicensePlate.getText().toString();
                    useraddress = tvAddress.getText().toString();
                    usermoney = Double.valueOf(strusermoney);

                    if (listUserData != null) {
                        listUserData.clear();
                    }

                    //anyToast(String.valueOf(listUserData.size()));
                    anyToast("Saved Info");
                    //push to fireBase in Save Button
                    UserData userAllData = new UserData(userid,username, useremail, userphone, usercarlicenseplate, useraddress, usermoney);
                    mUsersRef
                             .child(userid)
                            .setValue(userAllData);

                    //pass to Transaction

                    dia.dismiss();
                }
            });

            btnCancelInfo = mViewDiaInfo.findViewById(R.id.btnCancelInfo);
            btnCancelInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dia.dismiss();
                }
            });
            //endregion Info Button

            dia.show();
            //endregion edit info
        }
        if (view == btnEditTopUp) {
            anyToast("Edit Account");
            //TODO dialog money state
        }
        if (view == btnHistory){
            Intent i = new Intent(UserActivity.this,HistoryActivity.class);
            i.putExtra("userid",userid);
            startActivity(i);
        }
        if (view == btnLanguage) {
            anyToast("Language");
            //dialog language list include thai and english
        }
        if (view == btnLogOut) {
            //dialog reconfirm to log out
            final AlertDialog.Builder dia = new AlertDialog.Builder(this);
            dia.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dia.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    logoutProcess();
                    finishAffinity();
                    anyToast("Log out");
                }
            });
            dia.setTitle("Log out?");
            dia.setMessage("If you sure this app will log out");
            dia.show();
        }
    }
    //endregion optional

    private void logoutProcess() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        Intent intent = new Intent(UserActivity.this, LogInActivity.class);
        finishAffinity();  //Finish all stack
        startActivity(intent);
        //setResult Here
        finish();
    }

    //region lifeCycle
    @Override
    protected void onPause() {
        super.onPause();
        sp = getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putString("photourl",photourl);
        editor.putString("username",username);
        editor.putString("useremail",useremail);
        editor.putString("userphone",userphone);
        editor.putString("usercarlicenseplate",usercarlicenseplate);
        editor.putString("useraddress",useraddress);
        editor.putString("usermoney",strusermoney);
        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Todo DownLoad PlaceData From Database
        if (usermoney==null){}

        sp = getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        photourl = sp.getString("photourl",photourl);
        username = sp.getString("username",username);
        useremail = sp.getString("useremail",useremail);
        userphone = sp.getString("userphone",userphone);
        usercarlicenseplate = sp.getString("usercarlicenseplate",usercarlicenseplate);
        useraddress = sp.getString("useraddress",useraddress);
        strusermoney = sp.getString("usermoney",strusermoney);
        Picasso.with(this).load(photourl).into(imgUser);


        tvUserName.setText(username);
        tvEmail.setText(useremail);
        tvUserPhoneNo.setText(userphone);
        tvCarLicensePlate.setText(usercarlicenseplate);
        tvAddress.setText(useraddress);

    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot childing : dataSnapshot.getChildren()){
            ModelUserReceiveData modelU = childing.getValue(ModelUserReceiveData.class);
            Double doumoney = modelU.getUsermoney();
            String userid = modelU.getUserid();
            if (userid.equals(strUserId)){
                usermoney = doumoney;
                strusermoney = String.valueOf(usermoney);
            }
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
    //endregion LifeCycle

}
