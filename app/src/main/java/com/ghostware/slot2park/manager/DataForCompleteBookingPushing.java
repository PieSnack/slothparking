package com.ghostware.slot2park.manager;

public class DataForCompleteBookingPushing {
    public String placename
            ,price, datebookingstart, datebookingstop, timebookingstart, timebookingstop
            , strimageplaceurl,owner,ownerphone, username, usercarlicenseplate, userphone
            ,userid,urlmapgoogle;
    public int hoursparking;
    public DataForCompleteBookingPushing(String placename,
                                         String price,
                                         String datebookingstart,
                                         String datebookingstop,
                                         String timebookingstart,
                                         String timebookingstop,
                                         int hoursparking,
                                         String strimageplaceurl,
                                         String owner,
                                         String ownerphone,
                                         String username,
                                         String usercarlicenseplate,
                                         String userphone,
                                         String userid,
                                         String urlmapgoogle
    ) {
        this.placename = placename;
        this.price = price;
        this.datebookingstart = datebookingstart;
        this.datebookingstop = datebookingstop;
        this.timebookingstart = timebookingstart;
        this.timebookingstop = timebookingstop;
        this.hoursparking = hoursparking;
        this.strimageplaceurl = strimageplaceurl;
        this.owner = owner;
        this.ownerphone = ownerphone;
        this.username = username;
        this.usercarlicenseplate = usercarlicenseplate;
        this.userphone = userphone;
        this.userid = userid;
        this.urlmapgoogle = urlmapgoogle;
    }
}
