package com.ghostware.slot2park.utils;

public interface OnItemClickListener {

    void onItemClick(int position);
}
