package com.ghostware.slot2park.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ghostware.slot2park.R;
import com.ghostware.slot2park.adapter.RecyclerViewAdapterNotify;
import com.ghostware.slot2park.manager.DataBookingHistory;
import com.ghostware.slot2park.model.ModelReceiveToNoti;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class fmNotification extends Fragment implements View.OnClickListener {
    private TextView mTextView;
    private RecyclerView recyclerView;
    private RecyclerViewAdapterNotify mAdapter;
    private List<DataBookingHistory> notifyList;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mDataForNotify = mRootRef.child("CarParkingBooking");
    private String cUID;
    private View rootViewNotification;

    public fmNotification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootViewNotification = inflater.inflate(R.layout.fragment_fm_notification, container, false);
        initInstanceSetting(rootViewNotification);
        mDataForNotify.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                notifyList = new ArrayList<>();
                for(DataSnapshot korea : dataSnapshot.getChildren()){
                    ModelReceiveToNoti noti = korea.getValue(ModelReceiveToNoti.class);
                    String firebase_userid = noti.getUserid();
                    String placename = noti.getPlacename();
                    String price = noti.getPrice();
                    String datebookingstart = noti.getDatebookingstart();
                    String datebookingstop = noti.getDatebookingstop();
                    String timebookingstart = noti.getTimebookingstart();
                    String timebbokingstop = noti.getTimebookingstop();
                    int hours = noti.getHoursparking();
                    String owner = noti.getOwner();
                    String ownerphone = noti.getOwnerphone();
                    String userid = noti.getUserid();
                    String urlgooglemap = noti.getUrlmapgoogle();
                    String strimageplaceurl = noti.getStrimageplaceurl();

                    Log.d("uid", "onDataChange: "+firebase_userid);
                    if (firebase_userid.equals(cUID)) {

                        notifyList.add(new DataBookingHistory(placename,price,datebookingstart,datebookingstop,timebookingstart,timebbokingstop,hours,strimageplaceurl,owner,ownerphone,userid,urlgooglemap));
                        initRecyclerView(notifyList);

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }        });



        return rootViewNotification;
    }

    private void initRecyclerView(List<DataBookingHistory> notifyList) {
        recyclerView = rootViewNotification.findViewById(R.id.rvUpdateToday);

        mAdapter = new RecyclerViewAdapterNotify(notifyList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void initInstanceSetting(View view) {
        if (user!=null){
            cUID = user.getUid();
        }

    }


    @Override
    public void onClick(View view) {
    }

    private void anyToast(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

}
