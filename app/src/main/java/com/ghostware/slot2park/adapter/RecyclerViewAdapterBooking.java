package com.ghostware.slot2park.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ghostware.slot2park.R;
import com.ghostware.slot2park.utils.OnItemClickListener;
import com.ghostware.slot2park.manager.DataSearch;

import java.util.Collections;
import java.util.List;

public class RecyclerViewAdapterBooking extends RecyclerView.Adapter<ViewHolderBooking> implements View.OnClickListener {
    List<DataSearch> list = Collections.emptyList();

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public RecyclerViewAdapterBooking(List<DataSearch> list) {
        this.list = list;


    }


    @Override
    public ViewHolderBooking onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_booking, parent, false);
        v.setOnClickListener(this);
        return new ViewHolderBooking(v);

    }


    @Override
    public void onBindViewHolder(final ViewHolderBooking holder, int position) {
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(list.get(position).title);
        holder.description.setText(String.valueOf(list.get(position).stackremain));
        Glide.with(holder.imageView.getContext()).load(list.get(position).url).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(holder.getAdapterPosition());
                }
            }
        });
        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, DataSearch data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified PlaceData object
    public void remove(DataSearch data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onClick(View view) {
        //Toast.makeText(view.getContext(), "adapter click(Recycler_VIew_Adapter)", Toast.LENGTH_SHORT).show();

    }


}
