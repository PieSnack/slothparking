package com.ghostware.slot2park;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ghostware.slot2park.adapter.RecyclerViewAdapterHistory;
import com.ghostware.slot2park.manager.DataBookingHistory;
import com.ghostware.slot2park.model.ModelHistoryData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener, ValueEventListener {

    private ImageButton btnBackToUserActivity;
    private List<DataBookingHistory> listHistoryGet = new ArrayList<>();
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private String strUserId;
    private DatabaseReference mBookingHistoryRef;
    private ValueEventListener mBookingHistoryVEListener;
    private RecyclerViewAdapterHistory adapterHistoryBooking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        initInstance();
    }

    private void initInstance() {

        strUserId = getIntent().getStringExtra("userid");
        mBookingHistoryRef = mRootRef.child("CarParkingBooking");
        String dataKey = mBookingHistoryRef.child(strUserId).getKey();
        //anyToast(dataKey);

        dataGettingProcess();

        btnBackToUserActivity = (ImageButton) findViewById(R.id.imgBtnBackToUserActivity);
        btnBackToUserActivity.setOnClickListener(this);
    }

    private void dataGettingProcess() {
        mBookingHistoryRef.addValueEventListener(this);
    }

    private void anyToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view == btnBackToUserActivity) {
            finish();
        }
    }

    //region ve
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot childing : dataSnapshot.getChildren()) {
            ModelHistoryData modelHistoryData = childing.getValue(ModelHistoryData.class);

            String placename = modelHistoryData.getPlacename();
            String price = modelHistoryData.getPrice();
            String datebookingstart = modelHistoryData.getDatebookingstart();
            String datebookingstop = modelHistoryData.getDatebookingstop();
            String timebookingstart = modelHistoryData.getTimebookingstart();
            String timebookingstop = modelHistoryData.getTimebookingstop();
            int hoursparking = modelHistoryData.getHoursparking();
            String strimageplaceurl = modelHistoryData.getStrimageplaceurl();
            String owner = modelHistoryData.getOwner();
            String ownerphone = modelHistoryData.getOwnerphone();
            String userid = modelHistoryData.getUserid();
            String urlmapgoogle = modelHistoryData.getUrlmapgoogle();
            if (userid.equals(
                    strUserId)) {
                listHistoryGet.add(new DataBookingHistory(placename, price, datebookingstart
                        , datebookingstop, timebookingstart, timebookingstop,
                        hoursparking, strimageplaceurl, owner, ownerphone, userid,urlmapgoogle));
                Log.d("ninja", "onDataChange: " + userid+"hour="+hoursparking);

                RecyclerView rv = (RecyclerView) findViewById(R.id.rvHistory);
                rv.setHasFixedSize(true);
                LinearLayoutManager llm = new LinearLayoutManager(this);
                rv.setLayoutManager(llm);

                adapterHistoryBooking = new RecyclerViewAdapterHistory(listHistoryGet);
                rv.setAdapter(adapterHistoryBooking);
                adapterHistoryBooking.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        anyToast("No data");
    }
    //endregion ve


    @Override
    protected void onStop() {
        if (adapterHistoryBooking != null) {
            //ClearData
            mBookingHistoryRef.removeEventListener(this);
        }
        super.onStop();
    }
}
