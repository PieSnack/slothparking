package com.ghostware.slot2park.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class  ConnectivityReceiver extends BroadcastReceiver {

    public static ConnectivityReceiverListener connectivityReceiverListener;

    public ConnectivityReceiver() {
        super();
    }

    //
    @Override
    public void onReceive(Context context, Intent arg1) {
        ConnectivityManager cmNetWork = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cmNetWork.getActiveNetworkInfo();

        //Location
        LocationManager locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean loc = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        //boolean through isConnected
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting() && loc;

        //**connectivityReceiverListener
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }

    public static boolean isConnected(Context context) {
        //Connection
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        //Location
        LocationManager locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean loc = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected()&&loc);
    }



    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}