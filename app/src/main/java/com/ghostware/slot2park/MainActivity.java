package com.ghostware.slot2park;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghostware.slot2park.adapter.PagerFragments;
import com.ghostware.slot2park.fragment.fmQuickNav;
import com.ghostware.slot2park.manager.ConnectivityReceiver;
import com.ghostware.slot2park.manager.MyBroadcastReceiver;
import com.ghostware.slot2park.manager.PlaceData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private DatabaseReference mRootRef, mPlaceToPark;
    private StorageReference storageRef, storageImgPlaceRef;

    View viewMain;
    TabLayout tabLayout;
    ViewPager viewPager;
    ImageView imgView;
    TextView tvTitle;

    PagerFragments viewPGAdapter;
    //for pass intent
    private String strName,strUid,strEmail,strProviderId,strUserID,strPhotoUrl;
    private ByteArrayOutputStream byteArrayOutputStream;
    private Uri photoUrl;
    private int resultCodeMain;

    //pass Data to Fragment
    public void CallFromFragment(List<PlaceData> listData){
        //TODO pass listData from MainActivity to fmQuickNav
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());

        //log check
        if (listData!=null){
            Log.d("listdata",String.valueOf(listData.size()));
        }


        if (viewPager.getCurrentItem() == 0 && page != null) {
            ((fmQuickNav)page).MainToQuickNav(listData);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewMain = findViewById(android.R.id.content);
        setContentView(R.layout.activity_main);
        checkConnection();

        InitInstance();
        facebookAuthStatus();



    }

    private void facebookAuthStatus() {
        //recheck auth from facebook
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                strProviderId = profile.getProviderId();     //*
                strUid = profile.getUid();
                strName = profile.getDisplayName();
                photoUrl = profile.getPhotoUrl();
                strUserID = user.getUid();               //*
                strEmail = user.getEmail();              //*

                strPhotoUrl = "https://graph.facebook.com/" + strUid;

                //Picasso is a Library for manage the photo that might be larger from any source
                Picasso.with(this).load(photoUrl).into(imgView);

            }

        }
    }

    private void InitInstance() {
        imgView = (ImageView) findViewById(R.id.imgUserPicture);
        imgView.setAdjustViewBounds(true);
        imgView.setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPager.setOffscreenPageLimit(3);
        byteArrayOutputStream = new ByteArrayOutputStream();

        setUpTabLayout();
        setUpViewPager();
    }

    //region PagerControl
    private void setUpTabLayout() {
        //tabLayout.setupWithViewPager(viewPager);        //set tabLayout bind with viewPager

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_directions_car_black_24dp));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_bookmark_border_black_24dp));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_notifications_none_black_24dp));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        tvTitle.setText(R.string.parkingspot);

                        //custom icon here
                        break;
                    case 1:
                        tvTitle.setText(R.string.onvacation);
                        //custom icon here
                        break;
                    case 2:
                        tvTitle.setText(R.string.notifyme);
                        //custom icon here
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setUpViewPager() {
        viewPGAdapter = new PagerFragments(getSupportFragmentManager());
        viewPager.setAdapter(viewPGAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //viewPager.setPageTransformer(true, new CubeOutTransformer());            //Transformer Viewpager #Design

    }

    public void selectFragment(int position) {
        viewPager.setCurrentItem(position, true);
    // true is to animate the transaction
    }
    //endregion PagerControl

    //region Network and Location State
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        snackBarOnVersion(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(MainActivity.this);
            snackBarOnVersion(isConnected);

    }

    private void snackBarOnVersion(final boolean isConnected) {
        //TODO Test on android O or Geny Motion
        String messageForO;
        String message;
        if (isConnected) {

            message = "Connection established";

            Snackbar snackbar = Snackbar.make(viewMain, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.GREEN);
            snackbar.show();

            //btnQuickNav.setVisibility(View.VISIBLE);
        } else {
            messageForO = "No connection";
            message = "Please turn on Internet and Location";

            //region android O
            if (Build.VERSION.SDK_INT >= 26) {
                Snackbar snackbarForO = Snackbar.make(viewMain, messageForO, Snackbar.LENGTH_INDEFINITE);
                snackbarForO.setAction("Figure out", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LocationManager location = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            //TODO Test on API 26
                            if (!telephonyManager.isDataEnabled()) {
                                anyToast("Sync...Data Usage", Toast.LENGTH_SHORT);
                                telephonyManager.setDataEnabled(true);
                            }
                            if (!location.isProviderEnabled(LocationManager.GPS_PROVIDER) || !location.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                                anyToast("Location is on", Toast.LENGTH_SHORT);
                                location.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            }
                        }
                    }
                });
                snackbarForO.setActionTextColor(getColor(R.color.com_facebook_messenger_blue));
                snackbarForO.show();
            }
            //endregion android O

            //region lower than android O
            else if (Build.VERSION.SDK_INT < 26) {
                Snackbar snackbar = Snackbar.make(viewMain, message, Snackbar.LENGTH_INDEFINITE);
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
            //endregion lower than android O

        }
    }

    //endregion Network and Location State

    //region Life cycle
    @Override
    protected void onResume() {
        super.onResume();
        MyBroadcastReceiver.setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    //endregion Life cycle

    //region optional method
    private void anyToast(String s, int length) {
        Toast.makeText(this, s, length).show();
    }

    @Override
    public void onClick(View view) {
        if (view == imgView) {

            Intent intent = new Intent(this, UserActivity.class);
            intent.putExtra("username",strName);
            intent.putExtra("userid",strUserID);
            intent.putExtra("photourl",photoUrl.toString());
            intent.putExtra("useremail",strEmail);
            startActivity(intent);
            //this result received from UserActivity

        }
    }

    //endregion optional method


}//in MainActivity


