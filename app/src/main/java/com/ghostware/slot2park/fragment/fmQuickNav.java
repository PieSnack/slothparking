package com.ghostware.slot2park.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.constant.Unit;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.ghostware.slot2park.R;
import com.ghostware.slot2park.MainActivity;
import com.ghostware.slot2park.manager.PlaceData;
import com.ghostware.slot2park.manager.SimpleScanActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class fmQuickNav extends Fragment implements View.OnClickListener, OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener {
    private View rootViewQuickNav;
    GoogleMap gMap;
    MapView mapView;
    private Button btnQuickNav;
    private Button btnPagerSwipeToBooking;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LatLng mLatLng;

    private PlaceData thePlaceDataForUsage;
    public static final int REQUEST_LOCATION_CODE = 99;
    private int position;

    private Button btnSearchPlace;
    //PlaceData set From Main
    private Double douLat, douLng;
    private String strPlaceName, photURL;
    private String price;

    private PlaceData placeDataFromBooking;

    private String fakePlaceName = "SET";

    private List<PlaceData> lstPlaceData;
    private LatLng dLatLng;
    private Button btnQRScan;
    private float pinColorValue;

    public static fmQuickNav newInstance() {

        return new fmQuickNav();
    }

    //region get listPlace
    public void MainToQuickNav(List<PlaceData> listPlaceData) {
        lstPlaceData = new ArrayList<>();
        lstPlaceData = listPlaceData;
        Log.d("winter", "fragment0=" + String.valueOf(listPlaceData.size()));

        if (lstPlaceData != null) {
            pinMap();
        }
    }
    //endregion get listPlace

    public fmQuickNav() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        rootViewQuickNav = inflater.inflate(R.layout.fragment_fm_quicknav, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        initInStance(savedInstanceState);

        return rootViewQuickNav;
    }

    private void initInStance(Bundle savedInstanceState) {
        //Pager
        btnPagerSwipeToBooking = rootViewQuickNav.findViewById(R.id.btnPagerSwipeToBooking);
        btnPagerSwipeToBooking.setOnClickListener(this);
        mapView = rootViewQuickNav.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        //map start here
        mapView.getMapAsync(this);

        btnQuickNav = rootViewQuickNav.findViewById(R.id.btnQuickNav);
        btnQuickNav.setOnClickListener(this);

        btnQRScan = rootViewQuickNav.findViewById(R.id.btnQrScan);
        btnQRScan.setOnClickListener(this);
    }

    //region request direction and keep
    private void requestDirection() {

        if (lstPlaceData != null && mLatLng != null) {
            //Thanks for Lib from akexorcist
            Log.d("lstDataSize", String.valueOf(lstPlaceData.size()));
            //TODO Check best destination
            //animate Camera to current Location when finding...
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 16));

            String serverKey = getString(R.string.serverKeyGoogleDirectionAPI); //Server key
            Log.d("checkValue", serverKey);
            final LatLng origin = mLatLng;

            findBestIndex();

            final LatLng destination = dLatLng;
            Log.d("checkValue", String.valueOf(destination) + " | " + String.valueOf(thePlaceDataForUsage.title));

            GoogleDirection.withServerKey(serverKey)
                    .from(origin)                           //start point
                    .to(destination)                        //stop point
                    //TODO make a switch to custom as METRIC or IMPERIAL
                    .unit(Unit.METRIC)                      //kilometer
                    .avoid(AvoidType.FERRIES)               //avoid waterway
                    .transportMode(TransportMode.DRIVING)   //set available only for Car
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            anyToast("found the route!");
                            if (!direction.isOK()) {

                                Log.d("checkValue", direction.getErrorMessage());
                            }
                            Log.d("checkValue", String.valueOf(origin) + " | " + String.valueOf(destination));
                            if (direction.isOK()) {
                                //##############
                                ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
                                gMap.addPolyline(DirectionConverter.createPolyline(rootViewQuickNav.getContext(), directionPositionList, 5, Color.RED));


                            } else {
                                anyToast("found the route with problem");
                            }
                        }

                        @Override
                        public void onDirectionFailure(Throwable t) {
                            anyToast("No route found");
                        }
                    });
        } else {
            anyToast("No connection required");
        }
    }

    private LatLng findBestIndex() {
        Log.d("bestIndex", String.valueOf(lstPlaceData.size()));
        int bestIndex;
        LatLng eachLatLng;
        double originX = mLatLng.latitude, originY = mLatLng.longitude;
        Double douDistant = null;
        List<Double> lstDouDistantKeeper = new ArrayList<>();
        //calculate distant by ...
        int index;
        for (index = 0; index < lstPlaceData.size(); index++) {
            double destinationX = lstPlaceData.get(index).latitude,
                    destinationY = lstPlaceData.get(index).longitude;
            //    eachLatLng = new LatLng(destinationX,destinationY);

            douDistant = Math.sqrt((Math.pow(originX - destinationX, 2)) + (Math.pow(originY - destinationY, 2)));
            lstDouDistantKeeper.add(index, douDistant);

        }
        Log.d("show me distant", "lst Distant = " + String.valueOf(lstDouDistantKeeper));

        bestIndex = lstDouDistantKeeper.indexOf(Collections.min(lstDouDistantKeeper));
        Log.d("show me distant", "shortest is " + String.valueOf(Collections.min(lstDouDistantKeeper)) + " index = " + String.valueOf(bestIndex));

        //Double shortestDistant = lstDouDistantKeeper.get(bestIndex);
        dLatLng = new LatLng(lstPlaceData.get(bestIndex).latitude, lstPlaceData.get(bestIndex).longitude);

        return dLatLng;
    }

    //pinMap all PlaceToPark
    private void pinMap() {
        String strRemainSloth, strFullSloth;

        //this have to get from FireBase
        List<PlaceData> lstPlaceDataToMark;
        lstPlaceDataToMark = lstPlaceData;
        //TODO for each
        if (lstPlaceDataToMark != null) {
            //this loop for 5 times you can see the log below
            Log.d("pin", "Got place to Park");
            for (position = 0; position < lstPlaceData.size(); position++) {
                thePlaceDataForUsage = lstPlaceDataToMark.get(position);
                double centralLat = thePlaceDataForUsage.latitude;
                double centralLng = thePlaceDataForUsage.longitude;
                //looper to get value here
                strPlaceName = thePlaceDataForUsage.title;
                price = thePlaceDataForUsage.description;
                int intRemainLot = thePlaceDataForUsage.stackremain, intFullSloth = thePlaceDataForUsage.stackfull;
                int differentLot = Math.abs(intFullSloth - intRemainLot);
                strRemainSloth = String.valueOf(intRemainLot);
                strFullSloth = String.valueOf(intFullSloth);

                //status of pin
                if (intRemainLot > 10) {
                    pinColorValue = BitmapDescriptorFactory.HUE_AZURE;
                } else if (intRemainLot > 5) {
                    pinColorValue = BitmapDescriptorFactory.HUE_GREEN;
                } else if (intRemainLot > 3) {
                    pinColorValue = BitmapDescriptorFactory.HUE_YELLOW;
                } else if (intRemainLot > 1) {
                    pinColorValue = BitmapDescriptorFactory.HUE_ORANGE;
                } else {
                    pinColorValue = BitmapDescriptorFactory.HUE_RED;
                }


                gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(centralLat, centralLng))
                        .icon(BitmapDescriptorFactory.defaultMarker(pinColorValue))
                        .title(strPlaceName)
                        .snippet("Remain..." + strRemainSloth + "/" + strFullSloth)

                );

                gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return false;
                    }
                });

            }//end loop for


        } else {
            anyToast("Can not get any location.");
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onConnectionSuspended(int i) {
        anyToast("Connect Suspend");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(rootViewQuickNav.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    //endregion request direction and keep

    //region MapManagement
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // we have to set Pin here
        this.gMap = googleMap;



        buildGoogleApiClient();
        mGoogleApiClient.connect();
        if (ActivityCompat.checkSelfPermission(rootViewQuickNav.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(rootViewQuickNav.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        gMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        gMap.setMyLocationEnabled(true);
        gMap.setIndoorEnabled(true);

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(rootViewQuickNav.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(rootViewQuickNav.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000); //3 seconds
        mLocationRequest.setFastestInterval(0); //0 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        //Highest Zoom in
        gMap.setMaxZoomPreference(18);

        //Highest Zoom Out
        gMap.setMinZoomPreference(13);
        //***this will update location camera when move the camera
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        gMap.animateCamera(CameraUpdateFactory.newLatLng(mLatLng));

    }
    //endregion MapManagement

    //region options
    private void anyToast(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view == btnQuickNav) {
            requestDirection();

        }
        if (view == btnPagerSwipeToBooking) {
            ((MainActivity) getActivity()).selectFragment(1);
        }
        if (view == btnQRScan) {
            beginScanning();
        }

    }

    private void beginScanning() {
        Intent intent = new Intent(getActivity(), SimpleScanActivity.class);
        startActivity(intent);
    }

    //endregion options

    //region Life Cycle
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    //endregion Life Cycle

    //region Permission upper 23
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(rootViewQuickNav.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;

        } else
            return true;
    }


    //endregion Permission upper 23

}
