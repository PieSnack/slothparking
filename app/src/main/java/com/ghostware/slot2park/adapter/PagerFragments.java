package com.ghostware.slot2park.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ghostware.slot2park.fragment.fmQuickNav;
import com.ghostware.slot2park.fragment.fmBooking;
import com.ghostware.slot2park.fragment.fmNotification;

public class PagerFragments extends FragmentPagerAdapter {
    //check null

    public PagerFragments(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new fmQuickNav();
            case 1:
                return new fmBooking();
            case 2:
                return new fmNotification();
            default:
                return null;
        }
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}



