package com.ghostware.slot2park.model;

public class Province {
    private String province;



    public Province(String province){
        this.province=province;
    }


    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

}
