package com.ghostware.slot2park.model;

public class ModelPlaceData {


    String parkname;
    long price;
    double latitude, longitude;
    String photourl;
    Integer stackfull;
    Integer stackremain;
    String address;
    String owner;
    String ownerphone;


    public ModelPlaceData() {

    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerphone() {
        return ownerphone;
    }

    public void setOwnerphone(String ownerphone) {
        this.ownerphone = ownerphone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStackfull() {
        return stackfull;
    }

    public void setStackfull(Integer stackfull) {
        this.stackfull = stackfull;
    }

    public Integer getStackremain() {
        return stackremain;
    }

    public void setStackremain(Integer stackremain) {
        this.stackremain = stackremain;
    }

    public String getParkname() {return parkname;}
    public void setParkname(String parkname) {
        this.parkname = parkname;
    }

    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }

    public double getDouLatitude() {
        return latitude;
    }
    public void setDouLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getDouLongtitude() {
        return longitude;
    }
    public void setDouLongtitude(double longitude) {this.longitude = longitude;}

    public String getPhotourl() {return photourl;}
    public void setPhotourl(String photourl) {this.photourl = photourl;}

}



