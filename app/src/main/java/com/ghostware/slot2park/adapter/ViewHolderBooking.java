package com.ghostware.slot2park.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghostware.slot2park.R;

public class ViewHolderBooking extends RecyclerView.ViewHolder{
    TextView title;
    TextView description;
    ImageView imageView;

    ViewHolderBooking(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
        description = itemView.findViewById(R.id.description);
        imageView = itemView.findViewById(R.id.imgViewBookingPlace);
    }
}
