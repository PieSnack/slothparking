package com.ghostware.slot2park.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghostware.slot2park.R;
import com.ghostware.slot2park.model.Province;

import java.util.List;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.ProvinceViewHolder> {
    private List<Province> provinceList;

    public ProvinceAdapter(List<Province> provinceList) {
        this.provinceList = provinceList;
    }

    @Override
    public ProvinceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.province_list_row, parent, false);

        return new ProvinceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProvinceViewHolder holder, int position) {
        holder.province.setText(provinceList.get(position).getProvince());

    }

    @Override
    public int getItemCount() {
        return provinceList.size();
    }

    public class ProvinceViewHolder extends RecyclerView.ViewHolder {
        public TextView province;

        public ProvinceViewHolder(View view) {
            super(view);
            province = (TextView) view.findViewById(R.id.tvProvinceName);

        }
    }

}
