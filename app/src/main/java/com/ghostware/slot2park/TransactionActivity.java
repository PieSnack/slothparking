package com.ghostware.slot2park;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ghostware.slot2park.manager.DataForCompleteBookingPushing;
import com.ghostware.slot2park.manager.DataRepeatBookingMarker;
import com.ghostware.slot2park.model.ModelHistoryData;
import com.ghostware.slot2park.model.ModelPlaceData;
import com.ghostware.slot2park.model.ModelUserForTrans;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TransactionActivity extends AppCompatActivity implements View.OnClickListener, ValueEventListener {
    private TextView mTvPlaceName;
    private TextView mTvPrice;
    private String placename;
    private String price;
    private ImageButton imgBtnBackToMain;

    private ImageView imgViewPlace;

    private Button btnCheckInDate, btnCheckInTime, btnCheckOutDate, btnCheckOutTime;
    private Button btnOkTransaction;

    private Calendar calendar;

    private ImageView imgViewMapSheet;
    private String latPosting;
    private String lngPosting;
    private String strTimeStart, strTimeStop, strDateStart, strDateStop;

    private String strUserID;

    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mUserRef;
    private int position = 0;

    private TextView tvSuggestionBeforePay, tvPlaceNamePayForBooking, tvPricePayForBooking, tvTimeStart, tvTimeStop, tvDateStart, tvDateStop;
    private Double moneyValue;
    private int durationCheckInTime, durationCheckOutTime, valueCheckInD, valueCheckOutD;
    private String TAG = "timing";
    private Double cUserMoney;
    private String cUserName, cUserID, cUserEmail, cUserPhone, cUserCarLP, cUserAddress, strGotmoney, urlMapGoogle, strImagePlaceUrl, owner, ownerphone;
    private int minuteParking;
    private DatabaseReference mCurrentUserRef;
    private ValueEventListener mCurrentUserRefVEListener;
    private List<UserDataForTrans> listCurrentUser;
    private Double cUserGotmoney;
    private TextView tvCheckinAlert;
    private ImageButton imgBtnShowAttention;
    private TextView tvLotRemain;
    private DatabaseReference mPlaceRef;
    private LinearLayout tabAvailableToPay;
    private List<DataRepeatBookingMarker> listRepeatBookingMarker;

    public TransactionActivity() {
    }


    private void anyToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        initInstance();
        anySetting();
    }

    private void anySetting() {
        listRepeatBookingMarker = new ArrayList<>();
        imgBtnBackToMain.setOnClickListener(this);
        btnCheckInDate.setOnClickListener(this);
        btnCheckInTime.setOnClickListener(this);
        btnCheckOutDate.setOnClickListener(this);
        btnCheckOutTime.setOnClickListener(this);
        btnOkTransaction.setOnClickListener(this);
    }

    private void initInstance() {
        tabAvailableToPay = (LinearLayout) findViewById(R.id.tabAvailableToPay);
        mTvPlaceName = (TextView) findViewById(R.id.tvPlaceNameTransaction);
        mTvPrice = (TextView) findViewById(R.id.tvPriceTransaction);

        tvSuggestionBeforePay = (TextView) findViewById(R.id.tvSuggestionBeforePay);
        calendar = Calendar.getInstance();

        imgBtnBackToMain = (ImageButton) findViewById(R.id.imgBtnBackToMain);

        btnCheckInDate = (Button) findViewById(R.id.btnCheckInDate);
        btnCheckInTime = (Button) findViewById(R.id.btnCheckInTime);
        btnCheckOutDate = (Button) findViewById(R.id.btnCheckOutDate);
        btnCheckOutTime = (Button) findViewById(R.id.btnCheckOutTime);

        btnOkTransaction = (Button) findViewById(R.id.btnOkTransaction);
        imgViewMapSheet = (ImageView) findViewById(R.id.imgMapSheet);
        imgViewPlace = (ImageView) findViewById(R.id.imgViewOnTransaction);

        tvCheckinAlert = (TextView) findViewById(R.id.tvCheckInAlert);

        imgBtnShowAttention = (ImageButton) findViewById(R.id.imgBtnAttentionBookingForTrans);
        imgBtnShowAttention.setOnClickListener(this);

        tvLotRemain = (TextView) findViewById(R.id.tvLotRemain);
        dataGetting();
        mapProcess();

    }

    private void attentionDialog() {
        View mViewDiaAttentionBooking = getLayoutInflater().inflate(R.layout.dialog_attention_for_bookingfortrans, null);
        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setView(mViewDiaAttentionBooking);
        Button btnDoneAttention = mViewDiaAttentionBooking.findViewById(R.id.btnDoneReaddingAttention);
        dialog.show();
        btnDoneAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void dataGetting() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            strUserID = user.getUid();
        }
        mUserRef = mRootRef.child("UserProfile");
        mPlaceRef = mRootRef.child("PlaceToPark");
        currentPlaceData();
        currentUserData();
    }

    private void mapProcess() {
        latPosting = getIntent().getStringExtra("latitude");
        lngPosting = getIntent().getStringExtra("longitude");
        urlMapGoogle = "https://maps.googleapis.com/maps/api/staticmap?center=" + latPosting + "," + lngPosting + "&zoom=18&size=2000x350&maptype=roadmap&markers=color:red%7Clabel:" + placename + "%7C" + latPosting + "," + lngPosting + "&key=AIzaSyARYK6zHKqAnnAhAXgg7EDYUBxRH-kRW_c";
        Glide.with(this).load(urlMapGoogle).into(imgViewMapSheet);
    }

    private void currentPlaceData() {
        strImagePlaceUrl = getIntent().getStringExtra("imageUrl");
        Glide.with(this).load(strImagePlaceUrl).into(imgViewPlace);
        placename = getIntent().getStringExtra("placename");
        price = getIntent().getStringExtra("price");
        owner = getIntent().getStringExtra("owner");
        ownerphone = getIntent().getStringExtra("ownerphone");


        mTvPlaceName.setText(placename);
        String moneyUnit = price + " " + getString(R.string.unitMoney);

        if (price.equals("0")) {
            mTvPrice.setText("Free for parking");
        } else {
            mTvPrice.setText(moneyUnit);
        }

        mPlaceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    ModelPlaceData placeModel = snap.getValue(ModelPlaceData.class);
                    String placeName = placeModel.getParkname();
                    int remainLot = placeModel.getStackremain();
                    int fullLot = placeModel.getStackfull();
                    if (placeName.equals(placename)) {
                        if (remainLot == 0) {
                            tvLotRemain.setText("Sorry! This place is full.");
                            tabAvailableToPay.setVisibility(View.INVISIBLE);
                        } else {
                            tvLotRemain.setText("Remaining.." + String.valueOf(remainLot) + "/" + String.valueOf(fullLot));
                            tabAvailableToPay.setVisibility(View.VISIBLE);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void currentUserData() {
        //region cu
        mUserRef.addValueEventListener(this);

        cUserName = getIntent().getStringExtra("username");
        cUserID = getIntent().getStringExtra("userid");
        cUserEmail = getIntent().getStringExtra("useremail");

        if (listCurrentUser != null) {
            anyToast("listCurrentUser not null" + listCurrentUser.size());
            cUserGotmoney = listCurrentUser.get(0).usermoney;
        } else {
            cUserGotmoney = 30d;
        }

        cUserMoney = cUserGotmoney;
        //endregion cu
    }

    @Override
    public void onClick(View view) {
        if (view == imgBtnBackToMain) {
            finish();
        }
        if (view == imgBtnShowAttention) {
            attentionDialog();
        }

        if (view == btnCheckInDate) {
            inDateType("Check In Date", btnCheckInDate);
        }
        if (view == btnCheckInTime) {
            inTimeType("Check In Time", btnCheckInTime);
        }
        if (view == btnCheckOutDate) {
            inDateType("Check Out Date", btnCheckOutDate);
        }
        if (view == btnCheckOutTime) {
            inTimeType("Check Out Time", btnCheckOutTime);
        }
        //endregion calendar
        if (view == btnOkTransaction) {
            //TODO btnOkTransaction
            //putExtra

            strTimeStart = btnCheckInTime.getText().toString();
            strTimeStop = btnCheckOutTime.getText().toString();
            strDateStart = btnCheckInDate.getText().toString();
            strDateStop = btnCheckOutDate.getText().toString();

            //TODO


            if ((cUserPhone != null) && (cUserCarLP != null) && (cUserAddress != null)) {

                if (!(cUserPhone).equals("")                     //Phone Number Check
                        && !(cUserCarLP).equals("")                   //AddressCheck
                        && !(cUserAddress).equals(""))                 //CarLicense Plate Chec
                {
                    //placehere


                    //region CheckTime
                    if (!strTimeStart.equals("Time")
                            && !strTimeStop.equals("Time")
                            && !strDateStart.equals("Date")
                            && !strDateStop.equals("Date")) {
                        //region time
                        String[] unit_time_a = strTimeStart.split(":"); //will break the string up into an array
                        int hour_time_a = Integer.parseInt(unit_time_a[0]); //first element
                        int minute_time_a = Integer.parseInt(unit_time_a[1]); //second element
                        durationCheckInTime = (60 * hour_time_a) + minute_time_a; //add up our values


                        String[] unit_time_b = strTimeStop.split(":"); //will break the string up into an array
                        int hour_time_b = Integer.parseInt(unit_time_b[0]); //first element
                        int minute_time_b = Integer.parseInt(unit_time_b[1]); //second element
                        durationCheckOutTime = (60 * hour_time_b) + minute_time_b; //add up our values

                        //endregion time

                        moneyValue = Double.parseDouble(price) * ((durationCheckOutTime - durationCheckInTime) / 60);

                        //region date
                        String[] unit_date_a = strDateStart.split("/"); //will break the string up into an array
                        int day_date_a = Integer.parseInt(unit_date_a[0]); //first element
                        int month_date_a = Integer.parseInt(unit_date_a[1]); //second element
                        int year_date_a = Integer.parseInt(unit_date_a[2]);
                        valueCheckInD = day_date_a + (month_date_a * 30); //add up our values


                        String[] unit_date_b = strDateStop.split("/"); //will break the string up into an array
                        int day_date_b = Integer.parseInt(unit_date_b[0]); //first element
                        int month_date_b = Integer.parseInt(unit_date_b[1]); //second element
                        int year_date_b = Integer.parseInt(unit_date_b[2]);
                        valueCheckOutD = day_date_b + (month_date_b * 30); //add up our values

                        //endregion date

                        //condition prepare for check
                        //TODO check Formula
                        boolean yearDifOne = year_date_b - year_date_a <= 1;
                        //stop - start
                        minuteParking = (((hour_time_b * 60) + minute_time_b) + (24 * 60 * day_date_b) + (24 * 60 * 30 * month_date_b))
                                -
                                (((hour_time_a * 60) + minute_time_a) + (24 * 60 * day_date_a) + (24 * 60 * 30 * month_date_a));
                        int minuteMinimumAllow = ((hour_time_b - hour_time_a) * 60) + (24 * 60 * (day_date_b - day_date_a)) + (24 * 60 * 30 * (month_date_b - month_date_a)) + (minute_time_b - minute_time_a);
                        boolean minuteDifSixty = minuteParking
                                >=
                                60;

                        //Log.d("time tag", "parking min="+minuteParking);
                        //Log.d("time tag", "allow min="+minuteMinimumAllow);
                        long currentTimeDetect = ((60 * Calendar.getInstance().getTime().getHours()) + (Calendar.getInstance().getTime().getMinutes()) + 10);
                        long currentDateDetect = ((24 * 60 * Calendar.getInstance().getTime().getDay()) + (24 * 60 * 30 * Calendar.getInstance().getTime().getMonth()) + (12 * 24 * 30 * 60 * Calendar.getInstance().getTime().getYear()));

                        Log.d("cdatedetect", "onClick: " + currentDateDetect);

                        Log.d(TAG, "In Time : " + durationCheckInTime);
                        Log.d(TAG, "Out Time : " + durationCheckOutTime);
                        if (cUserMoney >= moneyValue) {
                            if (yearDifOne) {
                                if (valueCheckInD <= valueCheckOutD) {
                                    //unit as Minute
                                    if (valueCheckInD == valueCheckOutD) {//same day
                                        //TODO check date are on the current date or not


                                        if (durationCheckInTime < durationCheckOutTime) {
                                            if (durationCheckInTime > (currentTimeDetect)) {
                                                if (minuteDifSixty) {

                                                    //region dialog for payment control
                                                    View mViewDiaToPayForBook = getLayoutInflater().inflate(R.layout.dialog_payment, null);
                                                    final AlertDialog dialog = new AlertDialog.Builder(this).create();
                                                    dialog.setView(mViewDiaToPayForBook);
                                                    TextView tvCUserName, tvCUserPhoneNo, tvCUserCarLP;

                                                    //declare
                                                    //userend
                                                    tvCUserName = mViewDiaToPayForBook.findViewById(R.id.tvCUserName);
                                                    tvCUserName.setText(cUserName);

                                                    tvCUserPhoneNo = mViewDiaToPayForBook.findViewById(R.id.tvCUserPhoneNo);
                                                    tvCUserPhoneNo.setText(cUserPhone);

                                                    tvCUserCarLP = mViewDiaToPayForBook.findViewById(R.id.tvCUserCarLP);
                                                    tvCUserCarLP.setText(cUserCarLP);

                                                    //parkingend
                                                    tvPlaceNamePayForBooking = mViewDiaToPayForBook.findViewById(R.id.tvPlaceNameForBookingPay);
                                                    tvPricePayForBooking = mViewDiaToPayForBook.findViewById(R.id.tvPriceForBookingPay);
                                                    tvTimeStart = mViewDiaToPayForBook.findViewById(R.id.tvTimeStart);
                                                    tvTimeStop = mViewDiaToPayForBook.findViewById(R.id.tvTimeStop);
                                                    tvDateStart = mViewDiaToPayForBook.findViewById(R.id.tvDateStart);
                                                    tvDateStop = mViewDiaToPayForBook.findViewById(R.id.tvDateStop);
                                                    Button btnCancelPayForBook = mViewDiaToPayForBook.findViewById(R.id.btn_cancelPayForBook);
                                                    Button btnAgreePayForBook = mViewDiaToPayForBook.findViewById(R.id.btn_agreePayForBook);
                                                    //use

                                                    //in dialog
                                                    tvPlaceNamePayForBooking.setText(placename);
                                                    //TODO RealPrice equal price * hour

                                                    if (moneyValue != 0) {
                                                        tvPricePayForBooking.setText(String.valueOf(moneyValue) + " " + getString(R.string.unitMoney));
                                                    } else {
                                                        tvPricePayForBooking.setText("Free");
                                                    }

                                                    tvTimeStart.setText(strTimeStart);
                                                    tvTimeStop.setText(strTimeStop);
                                                    tvDateStart.setText(strDateStart);
                                                    tvDateStop.setText(strDateStop);
                                                    btnCancelPayForBook.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            dialog.dismiss();
                                                        }
                                                    });

                                                    btnAgreePayForBook.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            anyToast("Paid success");
                                                            //TODO push up the booking to firebase

                                                            updateUserInfoAfterPaid(moneyValue);

                                                            pushUpBookingToFireBase(tvPlaceNamePayForBooking.getText().toString(),
                                                                    String.valueOf(moneyValue),
                                                                    tvDateStart.getText().toString(),
                                                                    tvDateStop.getText().toString(),
                                                                    tvTimeStart.getText().toString(),
                                                                    tvTimeStop.getText().toString(),
                                                                    minuteParking,
                                                                    strImagePlaceUrl,
                                                                    owner,
                                                                    ownerphone,
                                                                    cUserName,
                                                                    cUserCarLP,
                                                                    cUserPhone, strUserID,urlMapGoogle
                                                            );

                                                            dialog.dismiss();
                                                        }
                                                    });
                                                    dialog.show();
                                                    //endregion dialog for payment control

                                                } else {
                                                    tvSuggestionBeforePay.setText("Minimum hour for Booking is 1 hour!");
                                                }


                                            } else {
                                                tvCheckinAlert.setText("Suggest 10 minute before park");
                                                tvSuggestionBeforePay.setText("Booking time should be 10 mins. from current time");
                                            }
                                        } else {
                                            tvSuggestionBeforePay.setText("Time is not correct!");
                                        }

                                        //TODO else check date are on the current date or not


                                    } else { //region case dif date
                                        if (durationCheckInTime > (currentTimeDetect)) {
                                            //different day with daybegin must less than dayfinish
                                            if (minuteDifSixty) {

                                                //region dialog for payment control
                                                View mViewDiaToPayForBook = getLayoutInflater().inflate(R.layout.dialog_payment, null);
                                                final AlertDialog dialog = new AlertDialog.Builder(this).create();
                                                dialog.setView(mViewDiaToPayForBook);
                                                TextView tvCUserName, tvCUserPhoneNo, tvCUserCarLP;

                                                //declare
                                                //userend
                                                tvCUserName = mViewDiaToPayForBook.findViewById(R.id.tvCUserName);
                                                tvCUserName.setText(cUserName);

                                                tvCUserPhoneNo = mViewDiaToPayForBook.findViewById(R.id.tvCUserPhoneNo);
                                                tvCUserPhoneNo.setText(cUserPhone);

                                                tvCUserCarLP = mViewDiaToPayForBook.findViewById(R.id.tvCUserCarLP);
                                                tvCUserCarLP.setText(cUserCarLP);

                                                //parkingend
                                                tvPlaceNamePayForBooking = mViewDiaToPayForBook.findViewById(R.id.tvPlaceNameForBookingPay);
                                                tvPricePayForBooking = mViewDiaToPayForBook.findViewById(R.id.tvPriceForBookingPay);
                                                tvTimeStart = mViewDiaToPayForBook.findViewById(R.id.tvTimeStart);
                                                tvTimeStop = mViewDiaToPayForBook.findViewById(R.id.tvTimeStop);
                                                tvDateStart = mViewDiaToPayForBook.findViewById(R.id.tvDateStart);
                                                tvDateStop = mViewDiaToPayForBook.findViewById(R.id.tvDateStop);
                                                Button btnCancelPayForBook = mViewDiaToPayForBook.findViewById(R.id.btn_cancelPayForBook);
                                                Button btnAgreePayForBook = mViewDiaToPayForBook.findViewById(R.id.btn_agreePayForBook);
                                                //use

                                                //in dialog
                                                tvPlaceNamePayForBooking.setText(placename);
                                                //TODO RealPrice equal price * hour

                                                if (moneyValue != 0) {
                                                    tvPricePayForBooking.setText(String.valueOf(moneyValue) + " " + getString(R.string.unitMoney));
                                                } else {
                                                    tvPricePayForBooking.setText("Free");
                                                }

                                                tvTimeStart.setText(strTimeStart);
                                                tvTimeStop.setText(strTimeStop);
                                                tvDateStart.setText(strDateStart);
                                                tvDateStop.setText(strDateStop);
                                                btnCancelPayForBook.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                btnAgreePayForBook.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {

                                                        anyToast("Paid success");
                                                        //TODO push up the booking to firebase

                                                        updateUserInfoAfterPaid(moneyValue);

                                                        pushUpBookingToFireBase(tvPlaceNamePayForBooking.getText().toString(),
                                                                String.valueOf(moneyValue),
                                                                tvDateStart.getText().toString(),
                                                                tvDateStop.getText().toString(),
                                                                tvTimeStart.getText().toString(),
                                                                tvTimeStop.getText().toString(),
                                                                minuteParking,
                                                                strImagePlaceUrl,
                                                                owner,
                                                                ownerphone,
                                                                cUserName,
                                                                cUserCarLP,
                                                                cUserPhone, strUserID,urlMapGoogle
                                                        );

                                                        dialog.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                                //endregion dialog for payment control

                                            } else {
                                                tvSuggestionBeforePay.setText("Minimum hour for Booking is 1 hour!");
                                            }
                                        } else {
                                            tvCheckinAlert.setText("Suggest 10 minute before park");
                                            tvCheckinAlert.setTextColor(getResources().getColor(R.color.colorAccent));
                                            tvSuggestionBeforePay.setText("Booking time should be 10 mins. from current time");
                                        }
                                        //endregion case dif date
                                    }
                                } else {
                                    tvSuggestionBeforePay.setText("Date or Time is not correct!");
                                }
                            } else {
                                tvSuggestionBeforePay.setText("Maximum Year of Booking is 1 year!");
                            }


                        } else {
                            tvSuggestionBeforePay.setText("Sorry! You don't have enough money");
                            Snackbar money_snack = Snackbar.make(view, "Go to Top up?", Snackbar.LENGTH_INDEFINITE);
                            money_snack.setAction("Top up", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent imoney = new Intent(TransactionActivity.this, UserActivity.class);
                                    imoney.putExtra("usermoney", cUserMoney);
                                    startActivity(imoney);
                                }
                            });
                            money_snack.show();
                        }


                    } else {
                        tvSuggestionBeforePay.setText("Please pick Date and Time");
                    }
                    //endregion chektime
                } else {

                    String strmissing = "Information";
                    if (cUserPhone.equals("")) {
                        strmissing = "Phone number";
                    }
                    if (cUserCarLP.equals("")) {
                        strmissing = "Car license plate";
                    }
                    tvSuggestionBeforePay.setText("We need your " + strmissing);
                    //region snack
                    Snackbar sn = Snackbar.make(view, "Not completely provide info?", Snackbar.LENGTH_LONG);
                    sn.setAction("Info", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(TransactionActivity.this, UserActivity.class);
                            i.putExtra("userid", cUserID);
                            i.putExtra("username", cUserName);
                            i.putExtra("useremail", cUserEmail);
                            i.putExtra("userphone", cUserPhone);
                            i.putExtra("usercarlicenseplate", cUserCarLP);
                            i.putExtra("useraddress", cUserAddress);
                            i.putExtra("usermoney", String.valueOf(cUserMoney));

                            startActivity(i);
                        }
                    });
                    sn.show();
                    //endregion snack
                }
            } else {
                //region snack
                Snackbar sn = Snackbar.make(view, "Not completely provide info?", Snackbar.LENGTH_LONG);
                sn.setAction("Info", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(TransactionActivity.this, UserActivity.class);
                        i.putExtra("userid", cUserID);
                        i.putExtra("username", cUserName);
                        i.putExtra("useremail", cUserEmail);
                        i.putExtra("userphone", cUserPhone);
                        i.putExtra("usercarlicenseplate", cUserCarLP);
                        i.putExtra("useraddress", cUserAddress);
                        i.putExtra("usermoney", String.valueOf(cUserMoney));

                        startActivity(i);


                    }
                });
                sn.show();
                //endregion snack
            }
        }
    }

    //TODO not Repeat Booking on same period booking
    private void pushUpBookingToFireBase(final String placename, final String price, final String dateBookingStart, final String dateBookingStop, final String timeBookingStart, final String timeBookingStop, final int hoursParking, final String strImagePlaceUrl, final String owner, final String ownerphone, final String cUserName, final String cUserCarLP, final String cUserPhone, final String strUserId, final String urlMapGoogle) {
        //TODO Check Lot Remain

        //TODO get Period and compared Push period

        String[] unit_date_a = dateBookingStart.split("/"); //will break the string up into an array
        int day_date_a = Integer.parseInt(unit_date_a[0]); //first element
        int month_date_a = Integer.parseInt(unit_date_a[1]); //second element
        int year_date_a = Integer.parseInt(unit_date_a[2]);
        String dateBegan = String.valueOf(day_date_a) + "-" + String.valueOf(month_date_a) + "-" + String.valueOf(year_date_a);

        String[] unit_date_b = dateBookingStop.split("/"); //will break the string up into an array
        int day_date_b = Integer.parseInt(unit_date_b[0]); //first element
        int month_date_b = Integer.parseInt(unit_date_b[1]); //second element
        int year_date_b = Integer.parseInt(unit_date_b[2]);
        String dateFinished = String.valueOf(day_date_b) + "-" + String.valueOf(month_date_b) + "-" + String.valueOf(year_date_b);

        String newBookingDateFormat = dateBegan + "to" + dateFinished;
        //************
        checkBookingHistory(placename, dateBookingStart, dateBookingStop, timeBookingStart, timeBookingStop);
        if (isBookNotRepeat()) {
            DatabaseReference mBookingRef = mRootRef.child("CarParkingBooking").child(strUserID + "(" + newBookingDateFormat + ")" + "(" + timeBookingStart + "to" + timeBookingStop + ")"); //strUserId+Date+Time
            DataForCompleteBookingPushing dataForCompleteBookingPushing = new DataForCompleteBookingPushing(placename, price, dateBookingStart, dateBookingStop, timeBookingStart, timeBookingStop, hoursParking, strImagePlaceUrl, owner, ownerphone, cUserName, cUserCarLP, cUserPhone, strUserId,urlMapGoogle);
            mBookingRef.setValue(dataForCompleteBookingPushing);

            completeDialog();
        } else {
            View mViewDialogRepeatBookingCase = getLayoutInflater().inflate(R.layout.dialog_asking_for_repeat_booking, null);
            final AlertDialog dia = new AlertDialog.Builder(this).create();
            dia.setView(mViewDialogRepeatBookingCase);
            Button btnCancel = mViewDialogRepeatBookingCase.findViewById(R.id.btnCancelBookingRepeatCase);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dia.dismiss();
                }
            });
            Button btnAgreeToBook = mViewDialogRepeatBookingCase.findViewById(R.id.btnBookItAnywayBookingRepeatCase);
            btnAgreeToBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseReference mBookingRef = mRootRef.child("CarParkingBooking").child(strUserID); //strUserId+Date+Time
                    DataForCompleteBookingPushing dataForCompleteBookingPushing = new DataForCompleteBookingPushing(placename, price, dateBookingStart, dateBookingStop, timeBookingStart, timeBookingStop, hoursParking, strImagePlaceUrl, owner, ownerphone, cUserName, cUserCarLP, cUserPhone, strUserId,urlMapGoogle);
                    mBookingRef.setValue(dataForCompleteBookingPushing);

                    completeDialog();
                }
            });
        }

    }

    private boolean isBookNotRepeat() {
        //endregion Date Pick
        boolean batch;
        if (listRepeatBookingMarker != null) {
            if (listRepeatBookingMarker.size() == 0) {
                anyToast("List Null Or Not" + String.valueOf(listRepeatBookingMarker.size()));
                batch = true;
            } else {
                anyToast(String.valueOf(listRepeatBookingMarker.size()));
                batch = false;
            }
        } else {
            batch = true;
        }
        return batch;
    }

    public void checkBookingHistory(final String placename, String dateBookingStart, String dateBookingStop, String timeBookingStart, String timeBookingStop) {
        final List<DataRepeatBookingMarker> listPullHistoryFromFirebase = new ArrayList<>();

        //region Time Pick
        String[] startTime
                = timeBookingStart.split(":"); //will break the string up into an array
        int hour_time_a = Integer.parseInt(startTime[0]); //first element
        int minute_time_a = Integer.parseInt(startTime[1]); //second element
        int startTimeAsInteger = (60 * hour_time_a) + minute_time_a; //add up our values

        String[] stopTime
                = timeBookingStart.split(":"); //will break the string up into an array
        int hour_time_b = Integer.parseInt(stopTime[0]); //first element
        int minute_time_b = Integer.parseInt(stopTime[1]); //second element
        int stopTimeAsInteger = (60 * hour_time_b) + minute_time_b; //add up our values
        //endregion Time Pick

        //region Date Pick
        String[] unit_date_a = dateBookingStart.split("/"); //will break the string up into an array
        int day_date_a = Integer.parseInt(unit_date_a[0]); //first element
        int month_date_a = Integer.parseInt(unit_date_a[1]); //second element
        int year_date_a = Integer.parseInt(unit_date_a[2]);
        long dateStart = (day_date_a * 24 * 60) + (month_date_a * 30 * 24 * 60) + (year_date_a * 12 * 30 * 24 * 60); //add up our values

        String[] unit_date_b = dateBookingStop.split("/"); //will break the string up into an array
        int day_date_b = Integer.parseInt(unit_date_b[0]); //first element
        int month_date_b = Integer.parseInt(unit_date_b[1]); //second element
        int year_date_b = Integer.parseInt(unit_date_b[2]);
        long dateStop = (day_date_b * 24 * 60) + (month_date_b * 30 * 24 * 60) + (year_date_b * 12 * 30 * 24 * 60); //add up our values

        final long startPoint = dateStart + startTimeAsInteger;
        final long stopPoint = dateStop + stopTimeAsInteger;

        DatabaseReference mCarParkingBooking = mRootRef.child("CarParkingBooking");


        mCarParkingBooking.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot history : dataSnapshot.getChildren()) {
                    ModelHistoryData model = history.getValue(ModelHistoryData.class);

                    String firebase_placename = model.getPlacename();
                    String firebase_timeStart = model.getTimebookingstart();
                    String firebase_timeStop = model.getTimebookingstop();
                    String firebase_dateStart = model.getDatebookingstart();
                    String firebase_dateStop = model.getDatebookingstop();
                    String firebase_userid = model.getUserid();

                    Log.d("getting", "onDataChange placename: " + firebase_placename);
                    Log.d("getting", "onDataChange timestart: " + firebase_timeStart);
                    Log.d("getting", "onDataChange timestop: " + firebase_timeStop);
                    Log.d("getting", "onDataChange datestart: " + firebase_dateStart);
                    Log.d("getting", "onDataChange datestop: " + firebase_dateStop);

                    if (firebase_userid.equals(strUserID)) {

                        if (firebase_dateStart != null) {
                            //region checking repeat

                            long bookedStart;
                            long bookedStop;

                            String[] unit_time_a = firebase_timeStart.split(":"); //will break the string up into an array
                            int hour_time_a = Integer.parseInt(unit_time_a[0]); //first element
                            int minute_time_a = Integer.parseInt(unit_time_a[1]); //second element
                            int checkinTime = (60 * hour_time_a) + minute_time_a; //add up our values

                            String[] unit_time_b = firebase_timeStop.split(":"); //will break the string up into an array
                            int hour_time_b = Integer.parseInt(unit_time_b[0]); //first element
                            int minute_time_b = Integer.parseInt(unit_time_b[1]); //second element
                            int checkoutTime = (60 * hour_time_b) + minute_time_b; //add up our values

                            String[] unit_date_a = firebase_dateStart.split("/"); //will break the string up into an array
                            int day_date_a = Integer.parseInt(unit_date_a[0]); //first element
                            int month_date_a = Integer.parseInt(unit_date_a[1]); //second element
                            int year_date_a = Integer.parseInt(unit_date_a[2]);
                            long checkindate = (24 * 60 * day_date_a) + (30 * 24 * 60 * month_date_a) + (24 * 30 * 60 * 12 * year_date_a); //add up our values


                            String[] unit_date_b = firebase_dateStop.split("/"); //will break the string up into an array
                            int day_date_b = Integer.parseInt(unit_date_b[0]); //first element
                            int month_date_b = Integer.parseInt(unit_date_b[1]); //second element
                            int year_date_b = Integer.parseInt(unit_date_b[2]);
                            long checkoutdate = (24 * 60 * day_date_b) + (30 * 24 * 60 * month_date_b) + (24 * 30 * 60 * 12 * year_date_b); //add up our values

                            bookedStart = checkinTime + checkindate;
                            bookedStop = checkoutTime + checkoutdate;

                            if (bookedStart <= startPoint && startPoint <= bookedStop) {
                                listPullHistoryFromFirebase.add(new DataRepeatBookingMarker(firebase_placename, firebase_dateStart, firebase_dateStop, firebase_timeStart, firebase_timeStop));
                                listRepeatBookingMarker = listPullHistoryFromFirebase;
                            } else {
                                if (bookedStart <= stopPoint && stopPoint <= bookedStop) {
                                    listPullHistoryFromFirebase.add(new DataRepeatBookingMarker(firebase_placename, firebase_dateStart, firebase_dateStop, firebase_timeStart, firebase_timeStop));
                                    listRepeatBookingMarker = listPullHistoryFromFirebase;


                                }
                            }

                        }
                    }

                }//end for loop
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        Log.d("tango", String.valueOf(listRepeatBookingMarker.size()));
    }

    private void updateUserInfoAfterPaid(Double moneyValue) {
        Double newCUserMoney = cUserMoney - moneyValue;
        //TODO push Money to FireBase
        mUserRef.child(strUserID).child("usermoney").setValue(newCUserMoney);
    }


    private void completeDialog() {
        final AlertDialog dia = new AlertDialog.Builder(this).create();
        View mViewComplete = getLayoutInflater().inflate(R.layout.dialog_complete, null);
        dia.setView(mViewComplete);
        Button btn_ok = mViewComplete.findViewById(R.id.btnOkComplete);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                dia.dismiss();
            }
        });


        dia.show();
    }

    private void inTimeType(final String str, final Button btn) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                btn.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                Log.d(TAG + "Type", str + selectedHour);
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setTitle(str);
        mTimePicker.show();
    }

    private void inDateType(final String str, final Button btn) {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                btn.setText(String.format("%02d/%02d/" + selectedYear, selectedDay, selectedMonth));
                Log.d(TAG + "Type", str + selectedDay);
            }
        }, year, month, day);
        mDatePicker.setTitle(str);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        listCurrentUser = new ArrayList<>();
        for (DataSnapshot oscar : dataSnapshot.getChildren()) {
            ModelUserForTrans model = oscar.getValue(ModelUserForTrans.class);
            String userid = model.getUserid();
            String username = model.getUsername();
            String useremail = model.getUseremail();
            String userphone = model.getUserphone();
            String usercarlicenseplate = model.getUsercarlicenseplate();
            String useraddress = model.getUseraddress();
            Double usermoney = model.getUsermoney();

            if (userid.equals(strUserID)) {
                listCurrentUser.add(new UserDataForTrans(userid, username, useremail, userphone, usercarlicenseplate, useraddress, usermoney));
                anyToast("inloop" + String.valueOf(listCurrentUser.size()));
                Log.d("vice", "onDataChange: =" + listCurrentUser.size());

                cUserPhone = listCurrentUser.get(0).userphone;
                cUserCarLP = listCurrentUser.get(0).usercarlicenseplate;
                cUserAddress = listCurrentUser.get(0).useraddress;

            }
        }

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
