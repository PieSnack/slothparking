package com.ghostware.slot2park.model;

public class ModelUserReceiveData {

    private String userid,
            username,
            useremail,
            userphone,
            usercarlicenseplate,
            useraddress;
    private Double usermoney;

    public ModelUserReceiveData() {

    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public String getUsercarlicenseplate() {
        return usercarlicenseplate;
    }

    public void setUsercarlicenseplate(String usercarlicenseplate) {
        this.usercarlicenseplate = usercarlicenseplate;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public Double getUsermoney() {
        return usermoney;
    }

    public void setUsermoney(Double usermoney) {
        this.usermoney = usermoney;
    }


}
