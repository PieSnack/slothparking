package com.ghostware.slot2park.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ghostware.slot2park.R;
import com.ghostware.slot2park.manager.DataBookingHistory;
import com.ghostware.slot2park.utils.OnItemClickListener;

import java.util.Calendar;
import java.util.List;

public class RecyclerViewAdapterNotify extends RecyclerView.Adapter<RecyclerViewAdapterNotify.NotifyViewHolder> implements View.OnClickListener {

    //constructor
    private List<DataBookingHistory> notifyList;

    public RecyclerViewAdapterNotify(List<DataBookingHistory> notifyList) {
        this.notifyList = notifyList;
    }

    @Override
    public void onClick(View view) {

    }
    //constructor

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public static class NotifyViewHolder extends RecyclerView.ViewHolder {
        //TODO
        CardView cvNotify;
        ImageView imgViewPlace;
        TextView tvStatus;
        TextView tvPlaceName;
        TextView tvTimeBegin;          //datebooking start
        TextView tvTimeFinish;
        ImageButton imgBtnReceipt;
        ImageButton imgBtnMap;
        ImageButton imgBtnContact;

        NotifyViewHolder(View itemView) {
            super(itemView);
            //TODO
            cvNotify = itemView.findViewById(R.id.cvNotify);
            imgViewPlace = itemView.findViewById(R.id.imgViewPlaceImage_rowUpdate);
            tvStatus = itemView.findViewById(R.id.tvRowUpdate);
            tvPlaceName = itemView.findViewById(R.id.tvPlaceNameRowUpdate);
            tvTimeBegin = itemView.findViewById(R.id.tvTimeBeginRowUpdate);
            tvTimeFinish = itemView.findViewById(R.id.tvTimeFinishRowUpdate);

            imgBtnReceipt = itemView.findViewById(R.id.imgBtnReceiptRowUpdate);
            imgBtnMap = itemView.findViewById(R.id.imgBtnMapRowUpdate);
            imgBtnContact = itemView.findViewById(R.id.imgBtnContactRowUpdate);
        }
    }

    //implement


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public NotifyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_updatetoday, parent, false);
        v.setOnClickListener(this);
        return new NotifyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(final NotifyViewHolder holder, final int position) {
        //TODO
        Glide.with(holder.imgViewPlace.getContext()).load(notifyList.get(position).strimageplceurl).into(holder.imgViewPlace);
        anyToast("onBindViewHolder: " + notifyList.get(position).datebookingstart, holder.cvNotify.getContext());
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int year = Calendar.getInstance().get(Calendar.YEAR);

        anyToast("onBindViewHolderCurrent: " + String.valueOf(day+"/"+month+"/"+year)
                //+"/"+String.valueOf(Calendar.MONTH)+"/"+String.valueOf(Calendar.YEAR)
                , holder.cvNotify.getContext());

        if (notifyList.get(position).datebookingstart.equals(String.valueOf(day+"/"+month+"/"+year))) {
            holder.tvStatus.setText("Today!");
            holder.tvStatus.setTextColor(holder.tvStatus.getResources().getColor(R.color.colorMightRedAlert));
        } else {

            holder.tvStatus.setText("On " + notifyList.get(position).datebookingstart);
            holder.tvStatus.setTextColor(holder.tvStatus.getResources().getColor(R.color.colorApp));
        }

        holder.tvPlaceName.setText(notifyList.get(position).placename);
        holder.tvTimeBegin.setText(notifyList.get(position).timebookingstart);
        holder.tvTimeFinish.setText(notifyList.get(position).timebookingstop);


        holder.imgBtnReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(holder.imgBtnReceipt.getContext(), "Receipt" + String.valueOf(position), Toast.LENGTH_SHORT).show();
                AlertDialog dia = new AlertDialog.Builder(holder.imgBtnReceipt.getContext()).create();
                //TODO
            }
        });
        holder.imgBtnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(holder.imgBtnReceipt.getContext(), "Map" + String.valueOf(position), Toast.LENGTH_SHORT).show();
                AlertDialog dia = new AlertDialog.Builder(holder.imgBtnReceipt.getContext()).create();
                //TODO

            }
        });
        holder.imgBtnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(holder.imgBtnReceipt.getContext(), "Contact" + String.valueOf(position), Toast.LENGTH_SHORT).show();
                AlertDialog dia = new AlertDialog.Builder(holder.imgBtnReceipt.getContext()).create();
                //TODO
            }
        });

    }

    private void anyToast(String s, Context v) {
        Toast.makeText(v, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return
                notifyList.size();
    }
    //implement
}
