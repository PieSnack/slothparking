package com.ghostware.slot2park;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogInActivity extends AppCompatActivity {
    private static final long SPLASH_DISPLAY_LENGTH = 5;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    private LoginButton loginButton;
    private CallbackManager facebookCallBackManager;

    String[] readPermission = new String[]{"email", "public_profile"};
    String TAG = "555";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initInstance();
        inAuthListener();
    }

    private void initInstance() {
        mAuth = FirebaseAuth.getInstance();
        facebookCallBackManager = CallbackManager.Factory.create();
        facebookLogin();
    }

    private void inAuthListener() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                            updateUI(user);
                    }
                };
                mAuth.addAuthStateListener(mAuthListener);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void facebookLogin() {
        loginButton = (LoginButton) findViewById(R.id.btnLoginFacebook);
        loginButton.setReadPermissions(readPermission);
        loginButton.registerCallback(facebookCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                anyToast("Success log in!");
                handleFacebookAccessToken(loginResult.getAccessToken());
                loginButton.setVisibility(View.GONE);
            }
            @Override
            public void onCancel() {
                anyToast("Cancel");
            }
            @Override
            public void onError(FacebookException error) {
                final AlertDialog alertDialog = new AlertDialog.Builder(LogInActivity.this).create();
                alertDialog.setTitle(R.string.alertTitle);
                alertDialog.setMessage("Something wrong");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information


                        } else {
                            // If sign in fails, display a message to the user.
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Must have to reply back to Facebook
        super.onActivityResult(requestCode, resultCode, data);
        facebookCallBackManager.onActivityResult(requestCode,resultCode,data);
    }

    //region life cycle and other
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setResult(0);

    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser!=null){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();}
    }

    private void anyToast(String str) {
        Toast.makeText(LogInActivity.this,str,Toast.LENGTH_SHORT).show();
    }
    //endregion life cycle and other

}