package com.ghostware.slot2park.manager;

public class UserData {
    String userid,username,useremail,userphone, usercarlicenseplate, useraddress;
    Double usermoney;

    public UserData(String userid, String username, String useremail, String userphone, String usercarlicenseplate, String useraddress, Double usermoney) {
        this.userid = userid;
        this.username=username;
        this.userphone = userphone;
        this.useremail = useremail;
        this.usercarlicenseplate = usercarlicenseplate;
        this.useraddress = useraddress;
        this.usermoney=usermoney;
    }

}
