package com.ghostware.slot2park.model;

public class ModelReceiveToNoti {
    public String placename;
    public String price;
    public String datebookingstart;
    public String datebookingstop;
    public String timebookingstart;
    public String timebookingstop;
    public int hoursparking;
    public String strimageplaceurl;
    public String owner;
    public String ownerphone;
    public String userid;
    public String urlmapgoogle;

    public ModelReceiveToNoti(){

    }

    public String getPlacename() {
        return placename;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDatebookingstart() {
        return datebookingstart;
    }

    public void setDatebookingstart(String datebookingstart) {
        this.datebookingstart = datebookingstart;
    }

    public String getDatebookingstop() {
        return datebookingstop;
    }

    public void setDatebookingstop(String datebookingstop) {
        this.datebookingstop = datebookingstop;
    }

    public String getTimebookingstart() {
        return timebookingstart;
    }

    public void setTimebookingstart(String timebookingstart) {
        this.timebookingstart = timebookingstart;
    }

    public String getTimebookingstop() {
        return timebookingstop;
    }

    public void setTimebookingstop(String timebookingstop) {
        this.timebookingstop = timebookingstop;
    }

    public int getHoursparking() {
        return hoursparking;
    }

    public void setHoursparking(int hoursparking) {
        this.hoursparking = hoursparking;
    }

    public String getStrimageplaceurl() {
        return strimageplaceurl;
    }

    public void setStrimageplaceurl(String strimageplaceurl) {
        this.strimageplaceurl = strimageplaceurl;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerphone() {
        return ownerphone;
    }

    public void setOwnerphone(String ownerphone) {
        this.ownerphone = ownerphone;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUrlmapgoogle() {
        return urlmapgoogle;
    }

    public void setUrlmapgoogle(String urlmapgoogle) {
        this.urlmapgoogle = urlmapgoogle;
    }
}
