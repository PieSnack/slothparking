package com.ghostware.slot2park.manager;

public class DataSearch {
    public String title;
    public String description;
    public String url;
    public Double latitude;
    public Double longitude;
    public Integer stackfull;
    public Integer stackremain;
    public String address;
    public String owner;
    public String ownerphone;

    public DataSearch(String title, String description, String photourl, Double latitude, Double longitude, Integer stackfull, Integer stackremain, String address,String owner,String ownerphone) {
        this.title = title;
        this.description = description;
        this.url = photourl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.stackfull = stackfull;
        this.stackremain = stackremain;
        this.address = address;
        this.owner=ownerphone;
    }


}

