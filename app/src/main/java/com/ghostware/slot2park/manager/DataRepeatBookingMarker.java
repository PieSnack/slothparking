package com.ghostware.slot2park.manager;

public class DataRepeatBookingMarker {

    String placeName;
    String dateStart;
    String dateStop;
    String timeStart;
    String timeStop;
    public DataRepeatBookingMarker(String placeName,String dateStart,String dateStop,String timeStart,String timeStop){
        this.placeName=placeName;
        this.dateStart=dateStart;
        this.dateStop=dateStop;
        this.timeStart=timeStart;
        this.timeStop =timeStop;
    }
}
