package com.ghostware.slot2park.manager;
import android.app.Application;


public class MyBroadcastReceiver extends Application {

        private static MyBroadcastReceiver mInstance;

        @Override
        public void onCreate() {
            super.onCreate();

            mInstance = this;
        }

        public static synchronized MyBroadcastReceiver getInstance() {
            return mInstance;
        }

        public static void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
            ConnectivityReceiver.connectivityReceiverListener = listener;
        }
    }

