package com.ghostware.slot2park.manager;

public class DataBookingHistory {
    public String placename;
    public String price;
    public String datebookingstart;
    public String datebookingstop;
    public String timebookingstart;
    public String timebookingstop;
    public int hours;
    public String strimageplceurl;
    public String owner;
    public String ownerphone;
    public String userid;
    public String urlmapgoogle;

    public DataBookingHistory(String placename, String price, String datebookingstart, String datebookingstop, String timebookingstart, String timebookingstop, int hours, String strimageplceurl, String owner, String ownerphone, String userid,String urlmapgoogle) {
        this.placename = placename;
        this.price = price;
        this.datebookingstart = datebookingstart;
        this.datebookingstop = datebookingstop;
        this.timebookingstart = timebookingstart;
        this.timebookingstop = timebookingstop;
        this.strimageplceurl = strimageplceurl;
        this.owner = owner;
        this.ownerphone = ownerphone;
        this.hours=hours;
        this.userid = userid;
        this.urlmapgoogle = urlmapgoogle;
    }

}
